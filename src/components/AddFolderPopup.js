import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import {Button} from '@material-ui/core'

class AddFolderPopup extends Component {
    state= {
        folderName: "",
        error: "action"
    }

    handlerChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        },() => console.log(this.state))
    }

    createFolder = () => {
        this.props.createFolder(this.state.folderName)
    }

    render() {
        return (
            <div>
                <Dialog open={this.props.open} onClose={this.props.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title"> Add Folder Name</DialogTitle>
                    <DialogContent>
                        <TextField
                            type="email"
                            margin="dense"
                            name="folderName"
                            value={this.state.folderName}
                            label="Enter Folder Name"
                            autoFocus
                            fullWidth
                            onChange={this.handlerChange}

                        />
                        <Button 
                            variant="contained" 
                            color="primary" 
                            fullWidth
                            onClick={this.createFolder}
                        >
                            Create Folder
                        </Button>
                        <div className="d-flex justify-content-start align-items-center">
                            <ErrorOutlineIcon 
                                color={this.state.error}
                                style={{fontSize: '20px'}}
                            />
                            <span style={{fontSize: '15px'}} >File Name Should be less than 30 Character</span>    
                        </div>

                    </DialogContent>
                    
                </Dialog>
            </div>
        );
    }
}

export default AddFolderPopup;