import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import FolderList from './FolderList/FolderList';
import SubFolder from './SubFolder/SubFolder'
import Header from './components/Header/Header';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter >
          <Header />
          <Switch>
            <Route path="/sub-folder/:id/:name" name="subFolder" component={SubFolder} />
            <Route exact path="/" name="Home" component={FolderList} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;