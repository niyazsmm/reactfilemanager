import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import FolderIcon from '@material-ui/icons/Folder';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import './SubFolder.css';

import AddFolderPopup from '../components/AddFolderPopup';
class SubFolder extends Component {

    state = {
        subFolderList: [],
        folderPopUp: false,

    }

    componentDidMount() {
        this.findChildFolder();
    }

    findChildFolder = () => {
        const childElementId = this.props.match.params.id;
        const folderList = JSON.parse(sessionStorage.getItem('folderList'));
        for(let i = 0; i < folderList.length; i++) {
            if(parseInt(childElementId) === parseInt(folderList[i].id)){
                if(folderList[i].child.length > 0){
                    this.setState({
                        subFolderList: folderList[i].child
                    })
                }
            }
        }
    }

    addFolderPopUp = (e) => {
        this.setState({
            folderPopUp: !this.state.folderPopUp
        })
    }

    createFolder = (name) => {
        const childElementId = this.props.match.params.id;
        const folderList = JSON.parse(sessionStorage.getItem('folderList'));
        const subFolderList = [...this.state.subFolderList];
        const folderObj = {
            id: subFolderList.length + 1,
            folderName: name,
        }
        subFolderList.push(folderObj);
        
        folderList[childElementId - 1].child = subFolderList;

        this.setState({
            subFolderList
        }, () => {
            sessionStorage.setItem('folderList', JSON.stringify(folderList));
            this.addFolderPopUp()
        })
    }


    render() {
        const { subFolderList } = this.state
        return (
            <div>
                <h1> Sub Folder List</h1>
                <div className="sub-folder-container">
                {
                    subFolderList.length > 0 ?
                        subFolderList.map((folder, index) => {
                            return(
                                <div className="sub-folder-list" key={index}>
                                    <div className="sub-folder-view">
                                        <Button>
                                            <FolderIcon 
                                                color="primary"
                                                style={{fontSize: '100px'}}
                                                
                                            />
                                        </Button>
                                        {folder.folderName}
                                    </div>
                                    {
                                        index === subFolderList.length - 1 ?
                                        <Button onClick={this.addFolderPopUp}>
                                            <CreateNewFolderIcon
                                                color="primary"
                                                style={{fontSize: '100px'}} 
                                            />
                                        </Button>
                                        :
                                        null
                                    }
                                </div>
                            )
                        })
                    :
                    <Button onClick={this.addFolderPopUp}>
                        <CreateNewFolderIcon
                            color="primary"
                            style={{fontSize: '100px'}} 
                        />
                    </Button>
                }
                </div>

                {
                    this.state.folderPopUp ?
                    <AddFolderPopup
                        open={this.state.folderPopUp}
                        handleClose={this.addFolderPopUp}
                        createFolder={(name) => this.createFolder(name)}
                    />
                    :
                    null
                }
            </div>
        );
    }
}

export default SubFolder;