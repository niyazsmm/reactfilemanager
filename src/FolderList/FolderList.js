import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import FolderIcon from '@material-ui/icons/Folder';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import AddFolderPopup from '../components/AddFolderPopup';
import './FolderList.css'
import { Link } from 'react-router-dom';

class FolderList extends Component {

    state = {
        folderList: [],
        folderPopUp: false,
    }

    componentDidMount() {
        if(sessionStorage.getItem('folderList')) {
            this.setState({
                folderList: JSON.parse(sessionStorage.getItem('folderList'))
            })
        }
    }

    // ===================== Modal Enable Code
    addFolderPopUp = (e) => {
        this.setState({
            folderPopUp: !this.state.folderPopUp
        })
    }

    //======================== Add Main Folder Code 
    createFolder = (name) => {
        console.log(name)
        const folderList = [...this.state.folderList];
        const folderObj = {
            id: folderList.length + 1,
            folderName: name,
            child:[],
        }
        folderList.push(folderObj);
        this.setState({
            folderList
        }, () => {
            sessionStorage.setItem('folderList', JSON.stringify(this.state.folderList));
            this.addFolderPopUp()
        })
    }

    render() {
        const { folderList } = this.state;
        return (
            <div>
                <h1>Folder List</h1>
                <div className="folder-container">
                {
                    folderList.length > 0 ?
                        folderList.map((folder, index) => {
                            return(
                                <div className="folder-list" xs="12" sm="2" md="2" lg="2" key={index}>
                                    <div className="folder-view">
                                        <Link to={`/sub-folder/${folder.id}/${folder.folderName}`}>
                                            <Button>
                                                <FolderIcon 
                                                    color="primary"
                                                    style={{fontSize: '100px'}}
                                                    
                                                />
                                            </Button>
                                        </Link>
                                        {folder.folderName}
                                    </div>
                                    {
                                        index === folderList.length - 1 ?
                                            <Button onClick={this.addFolderPopUp}>
                                                <CreateNewFolderIcon
                                                    color="primary"
                                                    style={{fontSize: '100px'}} 
                                                />
                                            </Button>
                                        :
                                        null
                                    }
                                </div>
                            )
                        })
                    :
                    <Button onClick={this.addFolderPopUp}>
                        <CreateNewFolderIcon
                            color="primary"
                            style={{fontSize: '100px'}} 
                        />
                    </Button>
                }
                </div>

                {
                    this.state.folderPopUp ?
                    <AddFolderPopup
                        open={this.state.folderPopUp}
                        handleClose={this.addFolderPopUp}
                        createFolder={(name) => this.createFolder(name)}
                    />
                    :
                    null
                }
            </div>
        );
    }
}

export default FolderList;